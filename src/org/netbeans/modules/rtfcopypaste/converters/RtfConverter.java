package org.netbeans.modules.rtfcopypaste.converters;

import java.awt.Color;
import javax.swing.JEditorPane;
import org.netbeans.modules.rtfcopypaste.utils.ConverterSettings;

public abstract class RtfConverter {

    public abstract String convertContentToRtf(JEditorPane pane);

    protected static String eol = System.getProperty("line.separator");

    protected String buildRtf(String fonttable, String colortable, String content) {
        Integer fontsize = ConverterSettings.getDefault().getSelectedFontSize() * 2;
        return buildRtf(fonttable, colortable, content, fontsize);
    }

    protected String buildRtf(String fonttable, String colortable, String content, Integer fontsize) {
        StringBuilder sb = new StringBuilder();
        sb.append("{\\rtf1\\ansi\\deff0 ").append(eol);
        sb.append(fonttable).append(eol);
        sb.append(colortable).append(eol);
        sb.append("\\f0\\fs").append(fontsize).append(eol);
        sb.append(content).append("}").append(eol);
        return sb.toString();
    }

    protected String fontTableRtf() {
        return "{\\fonttbl {\\f0 Courier New;}}";
    }

    protected String colorToRtf(Color color) {
        return "\\red" + color.getRed() + "\\green" + color.getGreen() + "\\blue"
                + color.getBlue();
    }

    protected final void emit(final StringBuilder sb, final String... strings) {
        for (String string : strings) {
            sb.append(string);
        }
    }

    protected final String tokenToRtf(final String oldcontent) {
        String content = escapeUtfCharsToRtf(oldcontent);
        StringBuilder sb = new StringBuilder(content.length());

        for (int i = 0; i < content.length(); i++) {
            sb.append(escapeCharToRtf(oldcontent.charAt(i)));
        }

        return sb.toString();
    }

    protected final String escapeCharToRtf(char c) {
        switch (c) {
            case '}':
                return "\\}";
            case '{':
                return "\\{";
            case '\n':
                return "\\par" + eol;
            case '\r':
                return "\\par" + eol;
            case '\t':
                return "\\tab";
            case ' ':
                return " ";
            default:
                return String.valueOf(c);
        }
    }

    private String escapeUtfCharsToRtf(final String oldcontent) {
        StringBuilder ret = new StringBuilder();

        for (char ch : oldcontent.toCharArray()) {
            if (ch == '\\') {
                ret.append("\\\\");
            } else if (ch < 0x7f) {
                ret.append(ch);
            } else {
                // convert UTF character to its ASCII representation "\ u<decimal code>?"
                ret.append("\\u");
                ret.append((int) ch);
                ret.append("?");
            }
        }
        return ret.toString();
    }
}
