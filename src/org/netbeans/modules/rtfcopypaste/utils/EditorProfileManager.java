package org.netbeans.modules.rtfcopypaste.utils;

import java.util.Set;

public abstract class EditorProfileManager {

    private static final EditorProfileManager MANAGER = new DefaultEditorProfileManager();

    public static EditorProfileManager getDefault() {
        return MANAGER;
    }

    public abstract Set<String> getFontAndColorsProfiles();

    public abstract String getCurrentFontAndColorsProfile();

    public abstract void setCurrentFontAndColorProfile(String profile);
}
